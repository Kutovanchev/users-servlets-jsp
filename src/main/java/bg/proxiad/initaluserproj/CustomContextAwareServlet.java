package bg.proxiad.initaluserproj;

import bg.proxiad.initialuserproj.services.UserService;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import org.springframework.context.ApplicationContext;

public abstract class CustomContextAwareServlet extends HttpServlet {
  /** */
  private static final long serialVersionUID = 1342244212675215774L;

  protected UserService userService;

  //  public ApplicationContext getApplicationContext() {
  //    return (ApplicationContext) getServletContext().getAttribute("applicationContext");
  //  }
  //
  //  public List<User> getUsers() {
  //    return getApplicationContext().getBean("users", UsersList.class).getUsers();
  //  }

  public void setUserService(UserService userService) {
    this.userService = userService;
  }

  @Override
  public void init(ServletConfig config) throws ServletException {
    // TODO Auto-generated method stub
    super.init(config);
    this.userService =
        ((ApplicationContext) config.getServletContext().getAttribute("applicationContext"))
            .getBean("userService", UserService.class);
  }
}
