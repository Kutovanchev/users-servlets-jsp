package bg.proxiad.initaluserproj;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@WebListener
public class ContextListener implements ServletContextListener {

  @Override
  public void contextInitialized(ServletContextEvent sce) {
    @SuppressWarnings("resource")
    ApplicationContext appContext = new ClassPathXmlApplicationContext("applicationContext.xml");
    sce.getServletContext().setAttribute("applicationContext", appContext);
    // appContext.getBean("users", UsersList.class).addAll(Seed.seedUsers());
    // sce.getServletContext().setAttribute("users", users);
  }
}
