package bg.proxiad.initaluserproj;

import bg.proxiad.initaluserproj.entities.User;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/users")
public class AllUsersServlet extends CustomContextAwareServlet {
  /** */
  private static final long serialVersionUID = 4298218423726282522L;

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    List<User> users = userService.getUsers();
    req.setAttribute("users", users);
    RequestDispatcher dispatcher = req.getRequestDispatcher("/list-users.jsp");
    dispatcher.forward(req, resp);
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    resp.sendRedirect("/users-servlets-jsp/users");
  }
}
