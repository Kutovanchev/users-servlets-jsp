package bg.proxiad.initaluserproj;

import bg.proxiad.initaluserproj.entities.User;
import java.io.IOException;
import java.util.Optional;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/edit/*")
public class EditUserServlet extends CustomContextAwareServlet {

  /** */
  private static final long serialVersionUID = 2274506136611954990L;

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {

    Optional<User> selectedUser =
        userService.selectedUser(Integer.valueOf(req.getPathInfo().substring(1).trim()));

    RequestDispatcher dispatcher = req.getRequestDispatcher("/edit-user.jsp");
    req.setAttribute("user", selectedUser.get());
    req.setAttribute("action", "/users-servlets-jsp/edit/*");
    req.setAttribute("method", "POST");
    req.setAttribute("btnValue", "Update User");
    dispatcher.forward(req, resp);
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
      throws IOException, ServletException {

    User editedUser =
        new User(req.getParameter("name"), req.getParameter("email"), req.getParameter("password"));
    editedUser.setId(Integer.valueOf(req.getParameter("id")));

    userService.editUser(editedUser);

    if (!resp.isCommitted()) resp.sendRedirect("/users-servlets-jsp/users");
  }
}
