package bg.proxiad.initaluserproj;

import java.io.IOException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/delete/*")
public class DeleteUserServlet extends CustomContextAwareServlet {
  /** */
  private static final long serialVersionUID = 8181506903880058002L;

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    userService.deleteUser(Integer.valueOf(req.getPathInfo().substring(1).trim()));
    resp.sendRedirect("/users-servlets-jsp/users");
  }
}
