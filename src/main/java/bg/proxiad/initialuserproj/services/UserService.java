package bg.proxiad.initialuserproj.services;

import bg.proxiad.initaluserproj.entities.User;
import bg.proxiad.initaluserproj.entities.UserRepository;
import java.util.List;
import java.util.Optional;

public class UserService {
  private UserRepository users;

  public void setUsers(UserRepository users) {
    this.users = users;
  }

  public void addUser(User user) {
    users.getUsers().add(user);
  }

  public List<User> getUsers() {
    return users.getUsers();
  }

  public void deleteUser(Integer userId) {
    for (User user : users.getUsers()) {
      if (user.getId() == userId) {
        users.getUsers().remove(user);
        break;
      }
    }
  }

  public Optional<User> selectedUser(Integer userId) {
    return users.getUsers().stream().filter(u -> u.getId() == userId).findFirst();
  }

  public void editUser(User editedUser) {
    for (User user : users.getUsers()) {
      if (user.getId() == editedUser.getId()) {
        user.setName(editedUser.getName());
        user.setEmail(editedUser.getEmail());
        user.setPassword(editedUser.getPassword());
      }
    }
  }
}
