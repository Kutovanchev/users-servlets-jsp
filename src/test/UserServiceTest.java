import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import bg.proxiad.initaluserproj.entities.Seed;
import bg.proxiad.initaluserproj.entities.User;
import bg.proxiad.initaluserproj.entities.UserRepository;
import bg.proxiad.initialuserproj.services.UserService;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class UserServiceTest {

  private UserService service;
  private List<User> users;

  @BeforeEach
  public void repoSetup() {
    System.out.println("before called");
    UserRepository mockRepository = mock(UserRepository.class);
    users = Seed.seedUsers();
    service = new UserService();
    service.setUsers(mockRepository);
    when(mockRepository.getUsers()).thenReturn(users);
  }

  @Test
  public void addUsertest() {
    User tempUser = new User("Mitaka2", "mitaka2@gmail.com", "mitaka2123");
    service.addUser(tempUser);
    assertTrue("The add to userList", service.getUsers().contains(tempUser));
  }

  @Test
  void getUsersTest() {
    assertArrayEquals(users.toArray(), service.getUsers().toArray());
  }

  @Test
  public void deleteUserTest() {
    service.deleteUser(1);
    assertTrue(!service.getUsers().stream().anyMatch(u -> u.getId() == 1));
  }
}
